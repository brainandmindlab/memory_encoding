#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Sep  3 12:15:09 2020

Get evoked potentials for all tasks

Ing.,Mgr. (MSc.) Jan Cimbálník
Biomedical engineering
International Clinical Research Center
St. Anne's University Hospital in Brno
Czech Republic
&
Mayo systems electrophysiology lab
Mayo Clinic
200 1st St SW
Rochester, MN
United States
"""


import os, re

import numpy as np
import pandas as pd
from scipy import signal

from bids import BIDSLayout, BIDSLayoutIndexer
from pymef import MefSession

import matplotlib.pyplot as plt


# %% Presets

institution = 'fnusa'
# institution = 'wroclaw'

path_to_dataset = f'/path/to/memory_datasets/{institution}_memory_dataset/'


subjects = [21, 22, 23, 24, 25, 26, 27, 28]
sessions = None
tasks = None
runs = None
channels = None


plt.switch_backend('Agg')

# %% Parse arguments
    
# parser = argparse.ArgumentParser(description='Memory encoding task detection of HFO')

# parser.add_argument('--subjects', default=None, action="store",
#                     nargs='+', type=int, help='Specify BIDS subject')
# parser.add_argument('--sessions', default=None, action="store",
#                     nargs='+',type=int, help='Specify BIDS session')
# parser.add_argument('--tasks', default=None, action="store",
#                     nargs='+',type=str, help='Specify BIDS task')
# parser.add_argument('--runs', default=None, action="store",
#                     nargs='+',type=int, help='Specify BIDS run')

# parser.add_argument('--channels', default=None, action="store",
#                     nargs='+',type=str, help='Specify iEEG channels run')

# parser.add_argument('--n_cores', default=None, action="store", type=int, help='Specify number of cores for multiprocessing')


# parsed_vals = parser.parse_args()

# subjects = parsed_vals.subjects
# sessions = parsed_vals.sessions
# tasks = parsed_vals.tasks
# runs = parsed_vals.runs

# channels = parsed_vals.channels

# n_cores = parsed_vals.n_cores

# %% Presets

path_to_source = path_to_dataset+'sourcedata/'
path_to_derivatives = path_to_dataset+'derivatives/'
os.makedirs(path_to_derivatives, exist_ok=True)

mef_pwd = None

pre_offset = 0.5  # in seconds
post_offset = 3.5  # in seconds

# Notch filter



# %% Processing - in small windows (2.5 seconds)
"""
The middle point for the window is:
COUNTDOWN - appearance of the countdown digit
ENCODING - appearance of the word(s)
DISTRACTOR - appearance of the equation
RECALL - start of word vocalization
"""

# Get data files
mefd_pattern = re.compile(r'\.mefd$')
indexer = BIDSLayoutIndexer(ignore=[mefd_pattern, 'sourcedata', 'derivatives', 'code'])
l = BIDSLayout(path_to_dataset, indexer=indexer, validate=True,
               database_path=path_to_dataset+'.sql', reset_database=True)
filter_dict = {'suffix': 'ieeg',
               'extension': 'json'}
if subjects is not None:
    filter_dict['subject'] = [str(x).zfill(3) for x  in subjects]
if sessions is not None:
    filter_dict['session'] = [str(x).zfill(3) for x in sessions]
if tasks is not None:
    filter_dict['task'] = tasks
if runs is not None:
    filter_dict['run'] = runs
json_files = l.get(**filter_dict)

mef_sessions = []
channel_dfs = []
event_dfs = []
for json_file in json_files:
    json_entities = json_file.entities
    
    # Get valid channels
    channel_file = l.get(suffix='channels', extension='tsv',
                          task=json_entities['task'],
                          subject=json_entities['subject'],
                          session=json_entities['session'],
                          run=json_entities['run'])[0]
    channel_df = channel_file.get_df()
    # channel_df = channel_df[channel_df['status'] == 'good']
    
    fs = channel_df['sampling_frequency'].values[0]
    
    channel_dfs.append(channel_df[((channel_df['type'].isin(['SEEG', 'ECOG']))
                                   & ~(channel_df['name'].str.startswith('Bm')))].reset_index())
    
    
    # Get events of interest
    events_file = l.get(suffix='events', extension='tsv',
                        task=json_entities['task'],
                        subject=json_entities['subject'],
                        session=json_entities['session'],
                        run=json_entities['run'])[0]
    events_df = events_file.get_df()
    if 'list' not in events_df.columns:
        events_df['list'] = 0
    events_df = events_df.loc[~events_df['trial_type'].isna(), ['onset', 'sample', 'duration', 'trial_type', 'list']]
    # During recall we want to analyze the start of vocalization - we need to correct for that in PAL task
    if json_entities['task'] == 'PAL':
        events_df.loc[events_df['trial_type'] == 'RECALL', 'sample'] += np.floor(events_df.loc[events_df['trial_type'] == 'RECALL', 'duration'] * fs).astype(int)
        events_df.loc[events_df['trial_type'] == 'RECALL', 'onset'] += events_df.loc[events_df['trial_type'] == 'RECALL', 'duration']
        
    """
    Limitation for events
    FR - only ENCODE
    PAL - only ENCODE
    AP - only saccades??
    SP - no selection
    """
    
    if json_entities['task'] in ['FR', 'PAL']:
        events_df = events_df[events_df['trial_type'] == 'ENCODE']
    if json_entities['task'] in ['AP']:
        events_df = events_df[events_df['trial_type'].isin(['antisaccade right', 'antisaccade left', 'prosaccade left', 'prosaccade right'])]
        
    event_dfs.append(events_df.loc[:, ['onset', 'sample', 'trial_type']])
    
    mef_session_path = os.path.splitext(json_file.path)[0]+'.mefd'
    mef_sessions.append(MefSession(mef_session_path, mef_pwd))

    
erp_channels = []

for ms, ch_df, ev_df, json_file in zip(mef_sessions, channel_dfs, event_dfs, json_files):

    cols =  10
    rows = (len(ch_df) // 10)+1
    
    f, ax_arr = plt.subplots(rows, cols, figsize=(24, 14), squeeze=False)
    ax_arr_1d = np.concatenate(ax_arr)
    
    print(f"Working on {json_file.filename[:-5]}")    
    
    json_entities = json_file.entities

    # Get uUTC unique times (PAL task can have the times doubled)
    uq_times = ev_df['onset'].unique()
    uq_times *= 1e6
    uq_uutc_times = (uq_times + ms.session_md['session_specific_metadata']['earliest_start_time'][0]).astype(int)
    uq_onsets = ev_df['onset'].unique()
    uq_samples = ev_df['sample'].unique()
        
    # Basic channel info
    bi = ms.read_ts_channel_basic_info()
    
    # Iterate over channels
    all_chan_df_list = []
    averaged_segments_list = []
    for i, ch in ch_df.iterrows():

        fs = ch['sampling_frequency']

        #uq_samples = uq_onsets * fs
        pre_samp_offset = int(pre_offset * fs)
        post_samp_offset = int(post_offset * fs)

        plt.sca(ax_arr_1d[i])
        plt.title(ch['name'])

        if channels is not None:
            if ch['name'] not in channels:
                continue

        if 'CSC' in ch['name'] or 'ECG' in ch['name']:
            continue
        
        # print(f"Plotting channel {ch}")
        
        data = ms.read_ts_channels_sample(ch['name'], [None, None])
        b, a = signal.iirnotch(50.0, 30.0, fs)
        data = signal.filtfilt(b, a, data)

        starts = [int(np.round(x-pre_samp_offset)) for x in uq_samples]
        stops = [int(np.round(x+post_samp_offset)) for x in uq_samples]
        
        start_stops = np.c_[starts, stops]

        print(f"{ch['name']}, fs {fs}, length ", stops[0]-starts[0])
        
        stim_data = np.zeros((start_stops.shape[0], pre_samp_offset+post_samp_offset), dtype='float32')
        for j, ss in enumerate(start_stops):
            if ss[0] > len(data):
                continue
            seg_data = data[ss[0]:ss[1]].copy()
                        
            # print(f"{ch['name']}, fs {fs}, seg {i} length ", len(seg_data))
                        
            # Eliminate offset
            
            seg_data -= np.mean(seg_data)
            
            if len(seg_data) < pre_samp_offset+post_samp_offset:
                continue
                        
            stim_data[j, :] = seg_data
            # plt.plot(stim_data[i], 'k', alpha=0.01)

        averaged_segments_list.append(np.mean(stim_data, 0))
        plt.plot(np.mean(stim_data, 0))
        plt.plot([pre_samp_offset, pre_samp_offset], plt.ylim())
        
        sub_seg = np.mean(stim_data, 0)[:2*fs]
        erp_present = False
        if (np.any(sub_seg > np.mean(sub_seg) + 3 * np.std(sub_seg))
            or np.any(sub_seg < np.mean(sub_seg) - 3 * np.std(sub_seg))):
            erp_present = True
        
        erp_channels.append([int(json_entities['subject']),
                            json_entities['run'],
                            json_entities['task'],
                            ch['name'],
                            erp_present])
        
        
        
    
    plt.suptitle(f"Subject {json_entities['subject']}, task {json_entities['task']}, run {json_entities['run']}")
    plt.tight_layout()

    
        
    # Plot mean segments
    plt.figure(figsize=(8,8))
    ax = plt.gca()
    ax.tick_params(axis='both', which='major', labelsize=12)
    for mean_seg in averaged_segments_list:
        sub_seg = mean_seg[:int(post_offset*fs)]
        
        erp_present = False
        if (np.any(sub_seg > np.mean(sub_seg) + 3 * np.std(sub_seg))
            or np.any(sub_seg < np.mean(sub_seg) - 3 * np.std(sub_seg))):
            erp_present = True
        
        if erp_present:
            # plt.plot(sub_seg, 'b', alpha=0.1)
            plt.plot(sub_seg, 'k', alpha=0.1)
        else:
            plt.plot(sub_seg, 'k', alpha=0.1)
        
    y_lim = plt.ylim()
    plt.plot([pre_samp_offset, pre_samp_offset], y_lim, 'k--')
    plt.plot([2*fs, 2*fs], y_lim, 'k:')
    plt.plot([3*fs, 3*fs], y_lim, 'k--')
    
    plt.xticks(np.arange(0, 4., 0.5)*fs, [x for x in [-0.5, 0, 0.5, 1.0, 1.5, 2.0, 2.5, 3.5]])
    
    plt.ylabel('Amplitude [uV]', fontsize=12)
    plt.xlabel('Times [s]', fontsize=12)
    plt.tight_layout()
    
df = pd.DataFrame(erp_channels, columns=['subject', 'run', 'task', 'channel', 'erp_present'])


        
     
