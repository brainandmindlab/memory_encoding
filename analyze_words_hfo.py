#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb  9 14:42:10 2022

Script to create plotly html to explore the WS dataset

Ing.,Mgr. (MSc.) Jan Cimbálník
Biomedical engineering
International Clinical Research Center
St. Anne's University Hospital in Brno
Czech Republic
&
Mayo systems electrophysiology lab
Mayo Clinic
200 1st St SW
Rochester, MN
United States
"""

import pickle, os

# Third party imports
import numpy as np
import pandas as pd
from scipy.stats import f_oneway, kruskal, ks_2samp, ranksums, ttest_rel, ttest_ind
from pandas.api.types import CategoricalDtype
import statsmodels.api as sm
from statsmodels.formula.api import ols

# import scikit_posthocs as sp

# from pymef import MefSession

import matplotlib.pyplot as plt
# from matplotlib.pyplot import figure
import seaborn as sns

# Local imports


# %% Presets

#sns.set_context('poster')
#sns.set
#sns.set(font_scale=2, rc={'figure.figsize':(15,10)})

plt.switch_backend('Agg')

stat = 'z-score'  # z-score or anova
montage = 'bipolar'

smooth_factor = 1

amp = 3
pre_stim = 0.5
post_stim = 1.5
win_size = post_stim
cycles = 3
freq = [80, 200]


fs = 5000

# Select institution (FNUSA, MAYO, WROCLAW)
institution = 'FNUSA'

path_to_datasets = '/media/jan_cimbalnik/ssd_data/memory_datasets/'

if institution == 'FNUSA':
    path_to_dataset_local = path_to_datasets+'fnusa_memory_dataset/'
    fs=5000
elif institution == 'MAYO':
    path_to_dataset_local = path_to_datasets+'mayo_memory_dataset/'
    fs=32000
elif institution == 'WROCLAW':
    path_to_dataset_local = path_to_datasets+'wroclaw_memory_dataset/'
    # Watch out, mixed frequencies!!!!
    fs=4000
else:
    raise ValueError(f"Unknown institution {institution}. Available institutions FNUSA, WRLOCLAW, MAYO")
# plt.switch_backend('Agg')

path_to_win_idx_data = f'/home/jan_cimbalnik/Nextcloud_work/Science/Projects/memory_encoding/pickles/{institution.lower()}/'

hfo_binned_all = pd.DataFrame()
# bins = np.arange(0, (post_stim+0.1)*1e6, 1e5)
bins = np.arange(pre_stim*1e6, (post_stim+0.1)*1e6, 1e5)

fnusa_memory_conversion_dict = {2: 116,
                                3: 117,
                                5: 119,
                                6: 120,
                                7: 121,
                                8: 122,
                                9: 123,
                                10: 125,
                                11: 126,
                                12: 130,
                                13: 131,
                                14: 132,
                                15: 133,
                                16: 136,
                                # 17: 138,
                                18: 139,
                                19: 141,} # 19 makes mess with run 1
                                #20: 142,} 20 makes mess with run 2 - check what is happening
                                #21: 143}

sig_run_marker_dict = {1: 's', 2: 'D', 3: 'X'}

# %% Load mni and anatomy tables

atlas = 'MNI-AAL3'
anat_df = pd.read_pickle(f'/home/jan_cimbalnik/Nextcloud_work/BME-neuro/metadata/{institution.lower()}_{montage}_brain_locations.pkl')
anat_df['hemisphere'] = anat_df[atlas].str[-1:]
anat_df[atlas] = anat_df[atlas].str[:-2]
anat_df.reset_index(inplace=True)
anat_df = anat_df.loc[:, ['subject', 'contact_name', 'x', 'y', 'z', atlas, 'MNI-BrodmannDilate', 'lobe']]

# Rename fields
anat_df = anat_df.reset_index().rename(columns={'contact_name': 'channel',
                                                atlas: 'structure',
                                                'MNI-BrodmannDilate': 'area'})

if institution == 'FNUSA':
    anat_df = anat_df.loc[anat_df['subject'].isin(fnusa_memory_conversion_dict.values())]
    anat_df['subject'] = anat_df['subject'].replace({y: x for x, y in fnusa_memory_conversion_dict.items()})


sig_channel_res_df = pd.read_pickle(f'/home/jan_cimbalnik/Nextcloud_work/Science/Projects/memory_encoding/pickles/temp/all_time_bin_sig_channels_inst-{institution}_mont-{montage}.pkl')
sig_bin_df = pd.read_pickle(f'/home/jan_cimbalnik/Nextcloud_work/Science/Projects/memory_encoding/pickles/temp/all_time_bin_sig_bins_inst-{institution}_mont-{montage}.pkl')

# TODO: Some channels are missing from MNI df - likely caused by omitting the last contact during conversion to BIDS - about to be fixed
all_sig_channels = sig_channel_res_df.merge(anat_df.loc[:, ['subject', 'channel']], on=('subject', 'channel'))

bin_counts_df = pd.read_pickle( f'/home/jan_cimbalnik/Nextcloud_work/Science/Projects/memory_encoding/pickles/temp/bin_counts_df_inst-{institution}_mont-{montage}.pkl')
bin_counts_df['time_bin'] = bin_counts_df['time_bin'].astype('category')
bin_counts_df = bin_counts_df.loc[bin_counts_df['subject'].isin(fnusa_memory_conversion_dict.keys())]
en_bin_counts_df = bin_counts_df.loc[bin_counts_df['trial_type'] == 'ENCODE']

# TODO: Make sure we discard the tasks and channels that could make a mess with our analysis (FR without responses, microelectrode channels, etc.)


# %% Helper function

def generate_word_idx_df(ws_df, word_idx_df):
    # Assign word order
    for word in word_idx_df['text'].unique():
        word_idx_df.loc[word_idx_df['text'] == word, 'pres_order'] = list([x+1 for x in range(5)])

    unique_chans = list(ws_df['channel'].unique())
    unique_chans.sort()

    word_idx_df = pd.concat([word_idx_df, pd.Series(np.tile(unique_chans, (len(word_idx_df),1)).tolist()).to_frame('channel')], axis=1)

    word_idx_ch_df = word_idx_df.explode('channel').reset_index(drop=True)
    
    return word_idx_ch_df

# %% Determine significant words for each subject/run/channel in ENCODE

# Create all_win_idx_events_df
pickle_files = os.listdir(path_to_win_idx_data)
all_win_idx_files = [x for x in pickle_files if 'win_idx' in x]
task_wind_idx_list = []
for f in all_win_idx_files:

    task_str = [x for x in f.split('_') if 'task' in x][0]
    win_idx_task = task_str.split('-')[1]
    task_wind_idx_list.append(pd.read_pickle(path_to_win_idx_data + f))

sig_chans = all_sig_channels.groupby(['subject', 'channel']).size().to_frame('n_bin_chans').reset_index()

# TODO - this can be done ex post!!!!
sig_en_bin_counts = en_bin_counts_df.merge(sig_chans[['subject', 'channel']])

all_win_idx_events_df = pd.concat(task_wind_idx_list)
sig_word_df_list = []
ws_response_words_df_list = []
for g, counts_df in sig_en_bin_counts.groupby(['subject', 'run', 'channel', 'task']):
    sub_win_idx_events_df = all_win_idx_events_df.loc[
        (all_win_idx_events_df['subject'] == g[0])
        & (all_win_idx_events_df['run'] == g[1])
        & (all_win_idx_events_df['task'] == g[3])
        & (all_win_idx_events_df['trial_type'] == 'ENCODE')]

    # Limit press order to 1 otherwise the grouping below will create surplus WORDS
    if g[3] in ['FR', 'PAL']:
        counts_df['pres_order'] = counts_df['pres_order'].astype(CategoricalDtype(categories=[1], ordered=False))

    # Get sum of HFOs per one showed word
    # TODO: observed=False or True?
    word_ch_count_df = counts_df[['text',
                                  'time_bin',
                                  'subject',
                                  'task',
                                  'run',
                                  'channel',
                                  'HFO_count',
                                  'pres_order']].groupby(
        ['text',
         'time_bin',
         'subject',
         'task',
         'run',
         'channel',
         'pres_order'], observed=False).sum().reset_index()
    word_ch_count_df['HFO_count'] = word_ch_count_df['HFO_count'].fillna(0)
    word_ch_count_df = word_ch_count_df.merge(anat_df, on=('channel', 'subject'))

    # We have to do this because task is a category
    word_ch_count_df = word_ch_count_df.loc[word_ch_count_df['task'] == g[3]]

    # ----- Channels -----
    word_ch_count_df['baseline_mean_std'] = False
    word_ch_count_df['ch_baseline_mean_std'] = False
    #word_ch_count_df['mean_std'] = False

    # Calculate baseline for the whole channel
    # IMPORTANT - we need need to cut the pre window short because WS has
    # 1.5 second distance between presentations so the previous word
    # influences the baseline!!!!!

    channel_baseline = word_ch_count_df.loc[(word_ch_count_df['time_bin'] <= 0) & (word_ch_count_df['time_bin'] >=-500000), 'HFO_count'].squeeze()
    ch_base_mean = np.mean(channel_baseline)
    ch_base_std = np.std(channel_baseline)
    # # calculate median of all bins
    # all_ch_hfo_counts = ch_df.loc[(ch_df['time_bin'] >=-500000), ['text', 'HFO_count']].groupby(['text'], as_index=False).sum()
    # word_count_median = all_ch_hfo_counts['HFO_count'].median()

    # Iterate over the words within the channel and mark the significant ones
    for word, word_df in word_ch_count_df.groupby(['text']):

        print(g, word)

        whole_vals = word_df.loc[(word_df['time_bin'] >=-500000), 'HFO_count'].squeeze()
        whole_mean = np.mean(whole_vals)
        whole_std = np.std(whole_vals)

        pre_vals = word_df.loc[(word_df['time_bin'] < 0) & (word_df['time_bin'] >=-500000), 'HFO_count'].squeeze()
        pre_mean = np.mean(pre_vals)
        pre_std = np.std(pre_vals)

        post_vals = word_df.loc[word_df['time_bin'] >= 0, 'HFO_count'].squeeze()

        # Prestimulus mean and std
        if any(post_vals > (pre_mean + 3 * pre_std)):
            word_ch_count_df.loc[(word_ch_count_df['text'] == word[0]), 'baseline_mean_std'] = True
            word_bl_sig = True
        else:
            word_bl_sig = True

        # Whole channel prestimulus mean and std
        if any(post_vals > (ch_base_mean + 3 * ch_base_std)):
            word_ch_count_df.loc[(word_ch_count_df['text'] == word[0]), 'ch_baseline_mean_std'] = True
            ch_bl_sig = True
        else:
            ch_bl_sig = False

        sig_word_df_list.append([g[0], g[1], g[2], g[3], word[0], word_bl_sig, ch_bl_sig])

    if g[3] == 'WS':
        ws_response_words_df_list.append(word_ch_count_df)

        # if any(post_vals > (whole_mean + 3*whole_std)):
        #     word_ch_count_df.loc[(word_ch_count_df['channel'] == ch[0])
        #                    & (word_ch_count_df['text'] == word[0]), 'mean_std'] = True


sig_word_df = pd.DataFrame(sig_word_df_list, columns=['subject', 'run', 'channel', 'task', 'text', 'word_baseline_significant', 'channel_baseline_significant'])
sig_word_df.to_pickle(f"/home/jan_cimbalnik/Nextcloud_work/Science/Projects/memory_encoding/pickles/temp/sig_word_df_inst-{institution}_mont-{montage}.pkl")

ws_response_words_df = pd.concat(ws_response_words_df_list)
ws_response_words_df.to_pickle(f"/home/jan_cimbalnik/Nextcloud_work/Science/Projects/memory_encoding/pickles/temp/ws_response_words_df_inst-{institution}_mont-{montage}.pkl")

# %% Question 1
# =============================================================================
#  Question 1 - how many channels are there and where are they.
#  Are they the same in WS and FR?
# =============================================================================

print("Working on question 1")

# Create an overview table
res_df = all_sig_channels.groupby(['structure', 'task']).size().to_frame('n_sig_channels').reset_index()
res_df['task'] = res_df['task'].astype(str)
res_df = res_df.loc[res_df['task'].isin(['WS', 'FR'])]
res_df = res_df[res_df['n_sig_channels'] > 0].reset_index()

# Normalize the counts by the number of contacts implanted in the particular structure and analyze by patient
res_df = all_sig_channels.groupby(['structure', 'subject', 'task'], observed=False).size().to_frame('n_sig_channels').reset_index()
res_df['task'] = res_df['task'].astype(str)
#res_df = res_df[res_df['n_sig_channels'] > 0].reset_index()


anova_df = res_df.loc[:, ['task', 'structure', 'n_sig_channels']]
anova_df['structure'] = anova_df['structure'].astype(str)

model = ols('n_sig_channels ~ C(task) + C(structure) + C(task):C(structure)', data=anova_df).fit()

two_way_anova_res = sm.stats.anova_lm(model, typ=2)


# Result interpretation:
# C(task)                 11.372180    1.0  6.395869  1.159328e-02
# C(structure)           250.481203   37.0  3.807406  6.494721e-13
# C(task):C(structure)    21.056391   37.0  0.320065  9.999688e-01
# Residual              1756.714286  988.0       NaN           NaN

# For p < 0.01
    # Structure has significant iimpact on sig channels
    # Task does not have significant impact on it
    # There is no significatn interaction between structure and task

sns.barplot(data=res_df, x='structure', y='n_sig_channels', hue='task')
ax = plt.gca()
ax.set_xticklabels(ax.get_xticklabels(), rotation = 45, ha='right')
plt.xlabel('Brain structure')
plt.ylabel('Number active channels')

plt.tight_layout()
plt.savefig(f'/home/jan_cimbalnik/Nextcloud_work/Science/Projects/memory_encoding/figures/{montage}/FR_WS_concordance_patient_institution-{institution}_stat-{stat}_mont-{montage}.png')
plt.close()

# =============================================================================
#  Answer 1 - Task does not have influence on channel counts p=0.36, anatomical location does p << 0.001, no interaction between task and structure p = 1
# =============================================================================

print("Done with question 1")


# %% Question 2
# =============================================================================
# Question 2 - test three models - Do words contribute roughly the same to the
# average response or do a subset of them drive the average response while most
# words show only little or no response? Is it different across the anatomy? 
# =============================================================================

# TODO: Discuss - whether we should use just run number 1 for this analysis
# TODO: Discuss - how doe we assess the peak of HFO occurence - whole segment or only oafter stimulation?

# Load data from previous analyses
# sig_word_df = pd.read_pickle(f"/home/jan_cimbalnik/Nextcloud_work/Science/Projects/memory_encoding/pickles/temp/sig_word_df_inst-{institution}_mont-{montage}.pkl")

# Create all_win_idx_events_df
pickle_files = os.listdir(path_to_win_idx_data)
all_win_idx_files = [x for x in pickle_files if 'win_idx' in x]
task_wind_idx_list = []
for f in all_win_idx_files:

    task_str = [x for x in f.split('_') if 'task' in x][0]
    win_idx_task = task_str.split('-')[1]
    task_wind_idx_list.append(pd.read_pickle(path_to_win_idx_data+f))
win_idx_events_df = pd.concat(task_wind_idx_list)
win_idx_events_df = win_idx_events_df.loc[win_idx_events_df['trial_type'] == 'ENCODE']

# Run through each subject/task/run/channel
channel_means_list_df = []
channel_mean_peak_list_df = []
channel_sig_word_prop_list_df = []


# ----- This is a part of analysis where I tried to get significant words from all channels not only significant ones
for g, counts_df in en_bin_counts_df.groupby(['subject', 'run', 'channel', 'task']):
    print(g)


    # Limit to only significant words from WS
    channel_words = sig_word_df.loc[(sig_word_df['subject'] == g[0])
                               & (sig_word_df['run'] == g[1])
                               & (sig_word_df['channel'] == g[2])
                               & (sig_word_df['task'] == 'WS'), ['text', 'word_baseline_significant', 'channel_baseline_significant']]
    sig_words = channel_words.loc[(sig_word_df['channel_baseline_significant'] == True), 'text']

    print(f"Working on subject {g[0]}, run {g[1]}, channel {g[2]}, task {g[3]}, number of sig words: {len(sig_words)}")

    sig_counts_df = counts_df.merge(sig_words)

    if len(sig_counts_df) == 0:
        continue

    # TODO: limit time to avoid WS being infuenced by surrounding words

    # Get mean response to stimuli in the channel
    channel_mean = sig_counts_df.groupby(['time_bin'], observed=False).agg({'HFO_count': 'mean'}).reset_index().fillna(0)
    channel_mean['channel'] = g[2]
    channel_mean['subject'] = g[0]
    channel_mean['run'] = g[1]
    channel_mean['task'] = g[3]
    channel_means_list_df.append(channel_mean)

    # Get mean response to stimuli for individual words
    channel_word_mean = sig_counts_df.groupby(['text', 'time_bin'], observed=False).agg({'HFO_count': 'mean'}).reset_index().fillna(0)
    channel_word_mean['channel'] = g[2]
    channel_word_mean['subject'] = g[0]
    channel_word_mean['run'] = g[1]
    channel_word_mean['task'] = g[3]

    # Calculate peak times
    channel_mean_peak = channel_mean.loc[[channel_mean.loc[channel_mean['time_bin'] >= 0, 'HFO_count'].idxmax()]]
    channel_mean_peak_list_df.append(channel_mean_peak)

    # Calculate proportion - how many words were active in the channel
    proportion = len(sig_words) / len(channel_words)
    channel_sig_word_prop_list_df.append([g[0], g[1], g[2], g[3], proportion])

for g, counts_df in en_bin_counts_df.loc[en_bin_counts_df['run'] == 1].groupby(['subject', 'run', 'task']):
    print(g)

    # Limit to only significant channels
    sig_chans = all_sig_channels.groupby(['subject', 'channel']).size().to_frame('n_bin_chans').reset_index()
    sub_task_counts_df = counts_df.merge(sig_chans[['subject', 'channel']])

    sub_win_idx_events_df = win_idx_events_df.loc[((win_idx_events_df['subject'] == g[0])
                                                   & (win_idx_events_df['run'] == g[1])
                                                   & (win_idx_events_df['task'] == g[2]))]

    # We need to account for zero detections in time bins
    if g[-1] in ['FR', 'PAL']:
        sub_task_counts_df['pres_order'] = sub_task_counts_df['pres_order'].astype(CategoricalDtype(categories=[1], ordered=False))

    word_ch_count_df = sub_task_counts_df[['text', 'time_bin', 'subject', 'channel', 'HFO_count', 'pres_order']].groupby(
        ['text', 'time_bin', 'subject', 'channel', 'pres_order']).sum().reset_index()
    word_ch_count_df['HFO_count'] = word_ch_count_df['HFO_count'].fillna(0)

    # TODO: why not do this at the beginning??? give it a try
    word_ch_count_df = word_ch_count_df.merge(anat_df, on=('channel', 'subject'))

    # ----- Channels -----
    # Get average response over individual channels - all words
    channel_means = word_ch_count_df.loc[:, ['subject', 'channel', 'time_bin', 'HFO_count']].groupby(['time_bin', 'channel'],
                                                                                          observed=True).agg(
        {'HFO_count': ['mean', 'std']}).reset_index()
    channel_means.columns = ['_'.join(col) if len(col[1]) > 1 else col[0] for col in channel_means.columns.values]
    channel_means.rename(columns={'HFO_count_mean': 'HFO_count'}, inplace=True)
    channel_means_list_df.append(channel_means)

    # Get average response over individual lobes - individual words
    channel_word_response = word_ch_count_df.loc[:, ['subject', 'text', 'channel', 'time_bin', 'HFO_count']].groupby(
        ['text', 'time_bin', 'channel'], observed=True).mean().reset_index().fillna(0)
    channel_word_response['subject'] = g[0]

    # !!!
    # IMPORTANT - we need need to cut the pre window short because WS has 1.5 second distance between presentations so the previous word influences the baseline!!!!!

    # Poststim bins > Baseline (in the given channel and word) mean + 3std
    word_ch_count_df['significant'] = False

    for ch, ch_df in word_ch_count_df.groupby(['channel'], observed=False):
        channel_baseline = ch_df.loc[(ch_df['time_bin'] <= 0) & (ch_df['time_bin'] >= -500000), 'HFO_count'].squeeze()
        ch_base_mean = np.mean(channel_baseline)
        ch_base_std = np.std(channel_baseline)
        print(ch)
        # calculate median of all bins
        all_ch_hfo_counts = ch_df.loc[(ch_df['time_bin'] >= -500000), ['text', 'HFO_count']].groupby(['text'],
                                                                                                     as_index=False).sum()
        word_count_median = all_ch_hfo_counts['HFO_count'].median()

        for word, word_df in ch_df.groupby(['text'], observed=False):

            # print('\t', word)

            whole_vals = word_df.loc[(word_df['time_bin'] >= -500000), 'HFO_count'].squeeze()
            whole_mean = np.mean(whole_vals)
            whole_std = np.std(whole_vals)

            pre_vals = word_df.loc[(word_df['time_bin'] < 0) & (word_df['time_bin'] >= -500000), 'HFO_count'].squeeze()
            pre_mean = np.mean(pre_vals)
            pre_std = np.std(pre_vals)

            post_vals = word_df.loc[word_df['time_bin'] >= 0, 'HFO_count'].squeeze()

            if any(post_vals > (ch_base_mean + 3 * ch_base_std)):
                word_ch_count_df.loc[(word_ch_count_df['channel'] == ch[0])
                                     & (word_ch_count_df['text'] == word[0]), 'significant'] = True

    # -------------------- FINSIEHED HERE WITH THE TRANSCRIPT ----------------


    # # Concat averages with individual word responses for easy later plotting
    # channel_word_response['is_average'] = False
    # channel_word_response = pd.concat([channel_word_response, channel_means])
    # channel_word_response.loc[channel_word_response['text'].isna(), 'is_average'] = True
    # # channel_word_response.loc[channel_word_response['text'].isna(), 'is_significant'] = False
    #
    # channel_word_means_list_df.append(channel_word_response)
    #
    # word_ch_count_df_list.append(word_ch_count_df)
    #
    # # ----- Move this part above to calculations and do not mix it with plotting
    #
    # peak_df = channel_word_response.loc[(channel_word_response['is_average'] == True)
    #                                     & (channel_word_response['time_bin'] >= 0), ['channel', 'HFO_count']].groupby(
    #     'channel', as_index=False).max()
    # peak_df = channel_word_response.loc[
    #     channel_word_response['is_average'] == True, ['channel', 'HFO_count', 'time_bin']].merge(peak_df)
    # peak_df['subject'] = subject
    #
    # word_peak_df = channel_word_response.copy()
    # word_peak_df['time_bin'] = word_peak_df['time_bin'].astype(int)
    # word_peak_df['text'] = word_peak_df['text'].astype(str)
    # word_peak_df = channel_word_response.merge(peak_df.loc[:, ['channel', 'time_bin']])
    #
    # word_peak_df = word_peak_df.loc[word_peak_df['text'] != 'AVERAGE']
    # word_peak_df = word_peak_df.merge(
    #     channel_word_response.loc[:, ['channel', 'HFO_count']].groupby('channel', as_index=False).mean().rename(
    #         columns={'HFO_count': 'mean_HFO_count'}))
    # word_peak_df = word_peak_df[word_peak_df['HFO_count'] > word_peak_df['mean_HFO_count']]
    #
    # prop_df = word_peak_df.groupby(['channel'], as_index=False).size().rename(columns={'size': 'n_contributing_words'})
    # prop_df['proportion'] = prop_df['n_contributing_words'] / 180
    # prop_df['subject'] = subject
    #
    # prop_df = prop_df.merge(anat_df)
    #
    # peak_df_list.append(peak_df.merge(anat_df))
    # props_at_peak_df_list.append(prop_df)




# Get final channel peak df and store it
channel_mean_peak_df = pd.concat(channel_mean_peak_list_df)
channel_mean_peak_df = channel_mean_peak_df.merge(anat_df)
channel_mean_peak_df.to_pickle(f"/home/jan_cimbalnik/Nextcloud_work/Science/Projects/memory_encoding/pickles/temp/channel_peak_df_inst-{institution}_mont-{montage}.pkl")

# Get final channel significant word proportion df and store it
channel_sig_word_prop_df = pd.DataFrame(channel_sig_word_prop_list_df, columns=['subject', 'run', 'channel', 'task', 'proportion'])
channel_sig_word_prop_df = channel_sig_word_prop_df.merge(anat_df)
channel_sig_word_prop_df.to_pickle(f"/home/jan_cimbalnik/Nextcloud_work/Science/Projects/memory_encoding/pickles/temp/channel_sig_word_prop_df_inst-{institution}_mont-{montage}.pkl")

# ----- Do HFO peak delay analysis in lobes -----
channel_peak_df = channel_mean_peak_df.groupby(['time_bin', 'lobe', 'task', 'channel'], observed=False).size().to_frame('peak_count').reset_index()
channel_peak_df = channel_peak_df.loc[channel_peak_df['time_bin'] >= 0]
channel_peak_df['time_bin'] = (channel_peak_df['time_bin'].astype(int)/1000000).astype('category')

g = sns.FacetGrid(channel_peak_df, row="task", height=4, aspect=2, sharey=False)
g.map_dataframe(sns.lineplot, x="time_bin", y="peak_count", hue='lobe')
g.add_legend()

plt.tight_layout()
plt.savefig(f'/home/jan_cimbalnik/Nextcloud_work/Science/Projects/memory_encoding/figures/{montage}/FR_WS_peak_occurence_line_institution-{institution.lower()}_mont-{montage}.png')
plt.close()

# Do statistics
anova_results_list = []
for time_bin, time_bin_df in channel_peak_df.groupby(['time_bin']):
    anova_samples = []

    for lobe, lobe_df in time_bin_df.groupby(['lobe']):
        anova_samples.append(lobe_df['peak_count'].astype('float').values)

    anova_res = f_oneway(*anova_samples)
    anova_results_list.append([time_bin[0], anova_res[0], anova_res[1]])

anova_results_df = pd.DataFrame(anova_results_list, columns=['time_bin', 'F', 'p'])

sns.lineplot(data=anova_results_df, x='time_bin', y='F')
plt.tight_layout()
plt.savefig(f'/home/jan_cimbalnik/Nextcloud_work/Science/Projects/memory_encoding/figures/{montage}/FR_WS_peak_delay_stats_institution-{institution.lower()}_mont-{montage}.png')
plt.close()

# ----- Do proportion analysis in individual lobes -----
sns.barplot(data=channel_sig_word_prop_df, x='lobe', y='proportion', hue='task')
ax = plt.gca()
ax.set_xticklabels(ax.get_xticklabels(), rotation = 45, ha='right')
plt.xlabel('Brain structure')
plt.ylabel('Proportion of significant words')

plt.tight_layout()
plt.savefig(f'/home/jan_cimbalnik/Nextcloud_work/Science/Projects/memory_encoding/figures/{montage}/FR_WS_sig_word_prop_bar_institution-{institution.lower()}_mont-{montage}.png')
plt.close()

# Do statistics
anova_samples = []
for lobe, lobe_df in channel_sig_word_prop_df.groupby(['lobe']):
    anova_samples.append(lobe_df['proportion'])

anova_res = f_oneway(*anova_samples)
print(anova_res)


# =============================================================================
# Answer 2 - Structure has significant role in HFO proportion one way ANOVA p << 0.001
# =============================================================================



# %% Question 3 / 4
# =============================================================================
# Question 3 - take a look at individual significant words - how they overlap 
# between WS and FR and PAL
# =============================================================================

# Load data from previous analyses
sig_word_df = pd.read_pickle(f"/home/jan_cimbalnik/Nextcloud_work/Science/Projects/memory_encoding/pickles/temp/sig_word_df_inst-{institution}_mont-{montage}.pkl")
ws_response_words_df = pd.read_pickle(f"/home/jan_cimbalnik/Nextcloud_work/Science/Projects/memory_encoding/pickles/temp/ws_response_words_df_inst-{institution}_mont-{montage}.pkl")

# Get ws significant words
sig_method = 'ch_baseline_mean_std'
ws_sig_word_df = sig_word_df.loc[(sig_word_df['task'] == 'WS'), :]
ws_sig_word_df.groupby(['subject', 'channel', 'text']).size()

# TODO: Is thi necessary???
ws_df = ws_response_words_df

# Limit to only significant channels
# ws_df = ws_df.merge(sig_channel_res_df.loc[sig_channel_res_df['task'] == 'WS', ['subject', 'channel']])
# fr_df = fr_df.merge(sig_channel_res_df.loc[sig_channel_res_df['task'] == 'FR', ['subject', 'channel']])

interation_df = sig_word_df.merge(anat_df)

structure_task_trial_df_list = []
area_task_trial_df_list = []
for group, group_df in bin_counts_df.groupby(['task', 'trial_type'], observed=False):
    print(group)


    # Inflate group_df
    bin_counts_df_list = []
    for subject_run, subject_run_df in group_df.groupby(['subject', 'run']):
        grouped_sub_df = subject_run_df[
            ['text', 'time_bin', 'subject', 'channel', 'HFO_count', 'pres_order', 'run']].groupby(
            ['text', 'time_bin', 'subject', 'channel', 'pres_order', 'run'], observed=False).sum().reset_index()
        grouped_sub_df = grouped_sub_df.merge(anat_df, on=('channel', 'subject'))
        bin_counts_df_list.append(grouped_sub_df)

    word_ch_count_df = pd.concat(bin_counts_df_list)
    word_ch_count_df['HFO_count'] = word_ch_count_df['HFO_count'].fillna(0)

    grouped_df = ws_df.loc[ws_df[sig_method]].groupby(['subject', 'channel', 'text'], as_index=False, observed=True).size()
    merged_df = word_ch_count_df.merge(grouped_df.loc[:, ['subject', 'channel', 'text']], how='left',
                                             indicator=True)

    merged_df[sig_method] = False
    merged_df.loc[merged_df['_merge'] == 'both', sig_method] = True
    merged_df = merged_df.merge(anat_df)

    merged_df['lobe'] = merged_df['lobe'].str.replace('n/a', 'na')

    """
    # Add information about active channels
    active_counts_df = group_df.loc[:, ['subject', 'run', 'channel', 'text', 'time_bin', 'HFO_count']].merge(
        ws_sig_word_df.loc[ws_sig_word_df['run'] == 1, ['subject', 'channel', 'text', sig_method]])

    active_counts_df = active_counts_df.merge(anat_df)
    # Change n/a lobe to na to avoid problems when saving figures
    active_counts_df.loc[active_counts_df['lobe'] == 'n/a', 'lobe'] = 'na'
    """

    # Iterate over lobes and plot individual structures and areas
    structure_time_bin_sig_df_list = []
    area_time_bin_sig_df_list = []
    for lobe, lobe_df in merged_df.groupby('lobe'):

        # ----- Structure -----
        g = sns.FacetGrid(lobe_df, col='structure', col_wrap=2, height=4, aspect=1, sharey=False)
        g.map_dataframe(sns.lineplot, data=lobe_df, x='time_bin', y='HFO_count', hue=sig_method, style='run')
        g.add_legend()

        for structure, strcture_df in lobe_df.groupby('structure', observed=False):
            ax = [x for x in g.axes if structure in x.title.get_text()][0]
            y_lim_max = ax.get_ylim()[1]
            for run, run_df in strcture_df.groupby('run', observed=False):
                for time_bin, time_bin_df in run_df.groupby('time_bin', observed=False):
                    sig_word_vals = time_bin_df.loc[time_bin_df[sig_method] == True, 'HFO_count'].values
                    nonsig_word_vals = time_bin_df.loc[time_bin_df[sig_method] == False, 'HFO_count'].values
                    s, p = ttest_ind(sig_word_vals, nonsig_word_vals)
                    structure_time_bin_sig_df_list.append([lobe, run, structure, time_bin, s, p])
                    if p < 0.05:
                        ax.plot(time_bin, y_lim_max, color='black',
                                marker=sig_run_marker_dict[run], linestyle='None')

                #y_lim_max = ax.get_ylim()[1]

        plt.suptitle(lobe)
        plt.tight_layout()
        plt.savefig(
            f'/home/jan_cimbalnik/Nextcloud_work/Science/Projects/memory_encoding/figures/{montage}/sig_non_sig_words_task_structures-{group[0]}-{group[1]}_stat-{stat}_mont-{montage}_sigmethod-{sig_method}_lobe-{lobe}.png')
        plt.close()

        # ----- Area -----
        g = sns.FacetGrid(lobe_df, col='area', col_wrap=2, height=4, aspect=1, sharey=False)
        g.map_dataframe(sns.lineplot, data=lobe_df, x='time_bin', y='HFO_count', hue=sig_method, style='run')
        g.add_legend()

        for area, area_df in lobe_df.groupby('area', observed=False):
            ax = [x for x in g.axes if area in x.title.get_text()][0]
            y_lim_max = ax.get_ylim()[1]
            for run, run_df in area_df.groupby('run', observed=False):
                for time_bin, time_bin_df in run_df.groupby('time_bin', observed=False):
                    sig_word_vals = time_bin_df.loc[time_bin_df[sig_method] == True, 'HFO_count'].values
                    nonsig_word_vals = time_bin_df.loc[time_bin_df[sig_method] == False, 'HFO_count'].values
                    s, p = ttest_ind(sig_word_vals, nonsig_word_vals)
                    area_time_bin_sig_df_list.append([lobe, run, area, time_bin, s, p])
                    if p < 0.05:
                        ax.plot(time_bin, y_lim_max, color='black',
                                marker=sig_run_marker_dict[run], linestyle='None')
                #y_lim_max = ax.get_ylim()[1]

        plt.suptitle(lobe)
        plt.suptitle(lobe)
        plt.tight_layout()
        plt.savefig(
            f'/home/jan_cimbalnik/Nextcloud_work/Science/Projects/memory_encoding/figures/{montage}/sig_non_sig_words_task_areas-{group[0]}-{group[1]}_stat-{stat}_mont-{montage}_sigmethod-{sig_method}_lobe-{lobe}.png')
        plt.close()

    structure_time_bin_sig_df = pd.DataFrame(structure_time_bin_sig_df_list,columns=['lobe', 'run', 'structure', 'time_bin', 'stat', 'p'])
    structure_time_bin_sig_df['task'] = group[0]
    structure_time_bin_sig_df['trial_type'] = group[1]
    structure_task_trial_df_list.append(structure_time_bin_sig_df)

    area_time_bin_sig_df = pd.DataFrame(area_time_bin_sig_df_list, columns=['lobe', 'run', 'area', 'time_bin', 'stat', 'p'])
    area_time_bin_sig_df['task'] = group[0]
    area_time_bin_sig_df['trial_type'] = group[1]
    area_task_trial_df_list.append(area_time_bin_sig_df)

all_structure_time_bin_sig_df = pd.concat(structure_task_trial_df_list)
all_area_time_bin_sig_df = pd.concat(area_task_trial_df_list)

all_structure_time_bin_sig_df.to_pickle(f"/home/jan_cimbalnik/Nextcloud_work/Science/Projects/memory_encoding/pickles/temp/acvtive_word_structure_time_bin_sig_df.pkl")
all_area_time_bin_sig_df.to_pickle(f"/home/jan_cimbalnik/Nextcloud_work/Science/Projects/memory_encoding/pickles/temp/acvtive_word_area_time_bin_sig_df.pkl")

# Labels sorted anterior to posterior (ChatGPT)
sorted_structure_labels = [
    'Frontal_Sup_Medial', 'Frontal_Sup_2', 'Frontal_Mid_2',
    'Frontal_Inf_Tri', 'Frontal_Inf_Orb_2', 'Frontal_Inf_Oper',
    'Precentral', 'Rectus', 'OFCant', 'OFClat', 'OFCmed', 'OFCpost',
    'Insula', 'Cingulate_Mid', 'Cingulate_Post', 'Rolandic_Oper',
    'Paracentral_Lobule', 'Postcentral', 'SupraMarginal', 'Parietal_Inf',
    'Parietal_Sup', 'Precuneus', 'Angular', 'Hippocampus',
    'ParaHippocampal', 'Amygdala', 'Fusiform', 'Heschl',
    'Temporal_Pole_Sup', 'Temporal_Pole_Mid', 'Temporal_Sup',
    'Temporal_Mid', 'Temporal_Inf', 'Lingual', 'Calcarine',
    'Cuneus', 'Occipital_Mid'
]

# Areas sorted anterior to posterior
sorted_brodmann_areas = [
    10, 9, 8, 6, 44, 45, 47, 11, 12, 32, 24, 25, 33,
    46, 4, 3, 1, 2, 5, 7, 31, 40, 39, 23, 29, 30, 26, 27, 28, 38,
    35, 36, 34, 20, 21, 22, 37, 19, 18, 17
]


# Create heatmaps

sns.set(font_scale=1.4)

# Structure
for group, group_df in all_structure_time_bin_sig_df.groupby(['task', 'trial_type'], observed=False):
    print('Heatmap structures', group)
    pivoted_df = group_df.pivot_table(index='structure', columns='time_bin', values='stat')
    pivoted_df = pivoted_df.loc[sorted_structure_labels, :]
    plt.figure(figsize=(10, 15))
    sns.heatmap(pivoted_df,
                vmin=group_df['stat'].quantile(q=0.1),
                vmax=group_df['stat'].quantile(q=0.9),
                cmap='crest',
                square=True)
    plt.show()
    plt.tight_layout()
    plt.savefig(f'/home/jan_cimbalnik/Nextcloud_work/Science/Projects/memory_encoding/figures/{montage}/heatmap_words_task_structures-{group[0]}-{group[1]}_stat-{stat}_mont-{montage}_sigmethod-{sig_method}.png')
    plt.close()

# Area
all_area_time_bin_sig_df.dropna(inplace=True)
all_area_time_bin_sig_df['area'] = all_area_time_bin_sig_df['area'].astype('category')
limited_sorted_brodmann_areas = [str(x) for x in sorted_brodmann_areas if str(x) in all_area_time_bin_sig_df['area'].unique()]
for group, group_df in all_area_time_bin_sig_df.groupby(['task', 'trial_type'], observed=False):
    print('Heatmap structures', group)
    pivoted_df = group_df.pivot_table(index='area', columns='time_bin', values='stat', dropna=False)
    pivoted_df = pivoted_df.loc[limited_sorted_brodmann_areas, :]
    plt.figure(figsize=(10, 15))
    sns.heatmap(pivoted_df,
                vmin=group_df['stat'].quantile(q=0.1),
                vmax=group_df['stat'].quantile(q=0.9),
                cmap='crest',
                square=True)
    plt.show()
    plt.tight_layout()
    plt.savefig(f'/home/jan_cimbalnik/Nextcloud_work/Science/Projects/memory_encoding/figures/{montage}/heatmap_words_task_areas-{group[0]}-{group[1]}_stat-{stat}_mont-{montage}_sigmethod-{sig_method}.png')
    plt.close()

# =============================================================================
# Answer 3 -
# =============================================================================
# =============================================================================
# Answer 4 -
# =============================================================================


# =============================================================================
# THE END
# =============================================================================







