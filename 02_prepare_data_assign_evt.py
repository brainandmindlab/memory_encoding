#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb 26 09:40:58 2020

Script to prepare data for all patients - basic thresholding and glueing
detections together

Ing.,Mgr. (MSc.) Jan Cimbálník
Biomedical engineering
International Clinical Research Center
St. Anne's University Hospital in Brno
Czech Republic
&
Mayo systems electrophysiology lab
Mayo Clinic
200 1st St SW
Rochester, MN
United States
"""

# Standard library imports

# Third party imports

# Local imports


import os, json
from bids.layout import BIDSLayout, BIDSLayoutIndexer
import numpy as np
import pandas as pd
from pymef import MefSession
from time import time


# %% Presets

# Select institution (FNUSA, MAYO, WROCLAW)
institution = 'fnusa'

path_to_datasets = ''

if institution == 'fnusa':
    path_to_dataset_local = path_to_datasets+'fnusa_memory_dataset/'
    fs=5000
elif institution == 'mayo':
    path_to_dataset_local = path_to_datasets+'mayo_memory_dataset/'
    fs=32000
elif institution == 'wroclaw':
    path_to_dataset_local = path_to_datasets+'wroclaw_memory_dataset/'
    # Watch out, mixed frequencies!!!!
    fs=4000
else:
    raise ValueError(f"Unknown institution {institution}. Available institutions fnusa, wroclaw, mayo")

path_to_pickle_dir = f""
os.makedirs(path_to_pickle_dir, exist_ok=True)

montage = 'unipolar'

path_to_derivatives = path_to_dataset_local+'derivatives/'+montage+'/'


pre_offset = 1.25 # in seconds
post_offset = 1.25  # in seconds

encode_pre_offset = 1.25
encode_post_offset = 1.25

recall_pre_offset = 1.25
recall_post_offset = 1.25

filter_dict = {'suffix': 'events',
               'task': ['FR', 'WS', 'PAL'],
               'extension': 'tsv'}

# Choose event type ['COUNTDOWN', 'ENCODE', 'DISTRACTOR', 'RECALL']
trial_types = ['ENCODE', 'RECALL']

# Thresholds section
# --- HFO thresholds ---

cyc_threshold = 2
threshold = 2.5
amp_threshold = threshold



# %% Load the layout

t = time()
indexer = BIDSLayoutIndexer(ignore=[r'\.mefd$','sourcedata','derivatives','code'])
l = BIDSLayout(path_to_dataset_local, indexer=indexer,
               database_path=path_to_dataset_local.rstrip('/')+'.sql', reset_database=True)

print(f'BIDS layout loaded in {time()-t}')


# %% Load the events dataframe

event_files = l.get(**filter_dict)
det_df_dict = {}
win_idx_events_df_list = []
det_df_list = []
electrodes_list = []


all_df = pd.DataFrame()
for event_file in event_files:
        
    entities = event_file.entities
    
    print(entities)

    # Load metadata from mef session
    filter_dict['suffix'] = 'ieeg'
    filter_dict['extension'] = 'json'
    filter_dict['subject'] = entities['subject']
    filter_dict['run'] = entities['run']
    filter_dict['task'] = entities['task']
    
    json_files = l.get(**filter_dict)
    json_file = json_files[0]
    
    mef_session_path = os.path.splitext(json_file.path)[0]+'.mefd'
    
    ms = MefSession(mef_session_path, None)

    events_df = event_file.get_df()

    # events_df = events_df.loc[~events_df['trial_type'].isna(), ['onset', 'sample', 'duration', 'trial_type', 'text', 'list']]
    events_df = events_df.loc[~events_df['trial_type'].isna(), ['onset', 'sample', 'duration', 'trial_type', 'text']]
    # During recall we want to analyze the start of vocalization - we need to correct for that in PAL task
    if entities['task'] == 'PAL':
        events_df.loc[events_df['trial_type'] == 'RECALL', 'sample'] += np.floor(events_df.loc[events_df['trial_type'] == 'RECALL', 'duration'] * fs).astype(int)
        events_df.loc[events_df['trial_type'] == 'RECALL', 'onset'] += events_df.loc[events_df['trial_type'] == 'RECALL', 'duration']
    
    # Load detections
    det_file_path = f"{path_to_derivatives}sub-{entities['subject']}_ses-{entities['session']}_run-{entities['run']}_task-{entities['task']}_th-{threshold}_mont-{montage}.pkl"
    if not os.path.exists(det_file_path):
        print(f"Detection:{det_file_path} file not found")
        continue
    det_df = pd.read_pickle(det_file_path)


    det_df['channel'] = det_df['channel'].astype('category')
    det_df['win_idx'] = det_df['win_idx'].astype('category')
    
    # Assign cycles
    det_df['cycles'] = det_df['freq_at_max'] * ((det_df['event_stop'] - det_df['event_start'])/1e6)
    
    # Remove detections that have the peak amplitude at the bottom (protection against spikes)
    det_df = det_df[~(det_df.freq_at_max == det_df.freq_min.min())]
    
    # Limit the whole dataset according to preset thresholds (grid search in the future (separate script)
    det_df = det_df.loc[det_df['max_amplitude'] > amp_threshold]
    det_df = det_df.loc[det_df['cycles'] > cyc_threshold]
    
    # !!! Watch out with PAL here - we are dropping the second word!!!
    uq_subset = events_df.drop_duplicates(subset="onset", keep='first').reset_index().copy()
    uq_subset['uutc_time'] = ((uq_subset['onset']*1e6) + ms.session_md['session_specific_metadata']['earliest_start_time'][0]).astype(int)

    # Eliminate entries from unique subset that do not overlap with the detections
    uq_subset = uq_subset.loc[(uq_subset['uutc_time'] >= det_df['event_start'].min()) & (uq_subset['uutc_time'] <= det_df['event_stop'].max())]
    uq_subset.reset_index(inplace=True, drop=True)

    # Assign win_idx back to the original event dataframe
    win_idx_events_df = events_df.merge(uq_subset.reset_index().rename(columns={'level_0': 'win_idx'}).loc[:, ['onset', 'win_idx', 'text']],
                                        on=['onset', 'text'])
    win_idx_events_df['subject'] = int(entities['subject'])
    win_idx_events_df['run'] = entities['run']
    win_idx_events_df['task'] = entities['task']
    
    win_idx_events_df_list.append(win_idx_events_df)
    
    # Map the detections to times
    # !!! This is a bit dangerous if the events or detections are not in order
    det_df = det_df.merge(uq_subset.loc[:, ['trial_type', 'uutc_time', 'text']], left_on='win_idx', right_index=True)
 

    det_df['relative_midpoint'] = ((det_df['event_start'] - det_df['uutc_time']) + (det_df['event_stop'] - det_df['uutc_time']))/2
    
    # Cut down to detections within the specified span    
    encode_idcs = (det_df['relative_midpoint'] >= -encode_pre_offset*1e6) & (det_df['relative_midpoint'] <= encode_post_offset*1e6) & (det_df['trial_type'] == 'ENCODE')
    recall_idcs = (det_df['relative_midpoint'] >= -recall_pre_offset*1e6) & (det_df['relative_midpoint'] <= recall_post_offset*1e6) & (det_df['trial_type'] == 'RECALL')
    countdown_idcs = (det_df['relative_midpoint'] >= -pre_offset*1e6) & (det_df['relative_midpoint'] <= post_offset*1e6) & (det_df['trial_type'] == 'COUNTDOWN')
    distractor_idcs = (det_df['relative_midpoint'] >= -pre_offset*1e6) & (det_df['relative_midpoint'] <= post_offset*1e6) & (det_df['trial_type'] == 'DISTRACTOR')

    det_df = det_df.loc[(encode_idcs) | (recall_idcs) | (countdown_idcs) | (distractor_idcs), :]
        
    det_df['task'] = entities['task']
    det_df['subject'] = int(entities['subject'])
    det_df['run'] = entities['run']
    det_df['trial_type'] = det_df['trial_type'].astype('category')
    det_df['channel'] = det_df['channel'].astype('str')
    det_df['win_idx'] = det_df['win_idx'].astype(int)
    det_df['task'] = det_df['task'].astype('category')

    det_df = det_df.loc[det_df['max_amplitude'] > threshold, :]
    win_idx_events_df['task'] = win_idx_events_df['task'].astype('category')


    det_df.to_pickle(
        f"{path_to_pickle_dir}inst-{institution}_sub-{entities['subject']}_run-{entities['run']}_task-{entities['task']}_montage-{montage}_th{str(threshold).replace('.', '')}.pkl")
    win_idx_events_df.to_pickle(
        f"{path_to_pickle_dir}inst-{institution}_sub-{entities['subject']}_run-{entities['run']}_task-{entities['task']}_montage-{montage}_win_idx_events_df.pkl")

    ms.close()
