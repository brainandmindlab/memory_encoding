#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb  9 14:42:10 2022

Script to create binned HFO counts and determine significant channels

Ing.,Mgr. (MSc.) Jan Cimbálník
Biomedical engineering
International Clinical Research Center
St. Anne's University Hospital in Brno
Czech Republic
&
Mayo systems electrophysiology lab
Mayo Clinic
200 1st St SW
Rochester, MN
United States
"""


# Third party imports
import json
import numpy as np
import pandas as pd
from scipy.stats import f_oneway

from bids import BIDSLayout, BIDSLayoutIndexer

# Local imports



# %% Helper function

def generate_word_idx_df(an_df, word_idx_df):
    # Assign word order
    
    if an_df['task'].unique()[0] in ['FR', 'PAL']:
        for word in word_idx_df['text'].unique():
            word_idx_df.loc[word_idx_df['text'] == word, 'pres_order'] = 1
    elif an_df['task'].unique()[0] == 'WS':
        for word in word_idx_df['text'].unique():
            word_idx_df.loc[word_idx_df['text'] == word, 'pres_order'] = list([int(x+1) for x in range(len(word_idx_df.loc[word_idx_df['text'] == word]))])

    unique_chans = list(an_df['channel'].unique())
    unique_chans.sort()

    word_idx_df = pd.concat([word_idx_df.reset_index(), pd.Series(np.tile(unique_chans, (len(word_idx_df),1)).tolist()).to_frame('channel')], axis=1)

    word_idx_ch_df = word_idx_df.explode('channel').reset_index(drop=True)
    
    return word_idx_ch_df

# %% Presets

threshold = 2.5
amp = 3
pre_stim = 1.2
post_stim = 1.2
win_size = post_stim
cycles = 3
freq = [80, 200]

fs = 5000

# Select institution (FNUSA, MAYO, WROCLAW)
institution = 'WROCLAW'

path_to_datasets = '/media/jan_cimbalnik/ssd_data/memory_datasets/'

if institution == 'FNUSA':
    path_to_dataset_local = path_to_datasets+'fnusa_memory_dataset/'
    fs=5000
elif institution == 'MAYO':
    path_to_dataset_local = path_to_datasets+'mayo_memory_dataset/'
    fs=32000
elif institution == 'WROCLAW':
    path_to_dataset_local = path_to_datasets+'wroclaw_memory_dataset/'
    # Watch out, mixed frequencies!!!!
    fs=4000
else:
    raise ValueError(f"Unknown institution {institution}. Available institutions FNUSA, WRLOCLAW, MAYO")

montage = 'bipolar'

atlas = 'MNI-AAL3'
anat_df = pd.read_pickle(f'/home/jan_cimbalnik/Nextcloud_work/BME-neuro/metadata/{institution.lower()}_{montage}_brain_locations.pkl')
anat_df['hemisphere'] = anat_df[atlas].str[-1:]
anat_df[atlas] = anat_df[atlas].str[:-2]
anat_df.reset_index(inplace=True)
anat_df = anat_df.loc[:, ['subject', 'contact_name', 'x', 'y', 'z', atlas, 'MNI-BrodmannDilate']]

# Rename fields
anat_df = anat_df.reset_index().rename(columns={'contact_name': 'channel',
                                                atlas: 'structure',
                                                'MNI-BrodmannDilate': 'area'})


bin_size = 1e5  # in microseconds
bins = np.arange(-pre_stim*1e6, (post_stim+0.1)*1e6, bin_size)

path_to_pickle_dir = f"/home/jan_cimbalnik/Nextcloud_work/Science/Projects/memory_encoding/pickles/{institution}/"

if institution == 'FNUSA':
    fnusa_memory_conversion_dict = {2: 116,
                                    3: 117,
                                    5: 119,
                                    6: 120,
                                    7: 121,
                                    8: 122,
                                    9: 123,
                                    10: 125,
                                    11: 126,
                                    12: 130,
                                    13: 131,
                                    14: 132,
                                    15: 133,
                                    16: 136,
                                    17: 138,
                                    18: 139}

    anat_df = anat_df.loc[anat_df['subject'].isin(fnusa_memory_conversion_dict.values())]
    anat_df['subject'] = anat_df['subject'].replace({y: x for x, y in fnusa_memory_conversion_dict.items()})

anova_sig_channels_list_df = []
time_bin_sig_list_bins_df = []
time_bin_sig_channels_list_df = []
sig_channels_words_df = []

bin_counts_list_df = []

indexer = BIDSLayoutIndexer(ignore=[r'\.mefd$','sourcedata','derivatives','code'])
l = BIDSLayout(path_to_dataset_local, indexer=indexer,
               database_path=path_to_dataset_local.rstrip('/')+'.sql', reset_database=True)

filter_dict = {'suffix': 'ieeg',
               'extension': 'json',
               'task': ['WS', 'FR']}


for bids_file in l.get(**filter_dict):
    
    entities = bids_file.entities
    
    subject = int(entities['subject'])
    
    # Remove subjects with artifacts
    # Patient 14 FR  - artifacts
    if subject in [14]:
        continue

    print(f"Subject {subject}")
    
    # %% Load events
    
    evt_filter = {'suffix': 'events',
                  'extension': 'tsv',
                  'subject': entities['subject'],
                  'task': entities['task'],
                  'run': entities['run']}
    
    events = l.get(**evt_filter)[0].get_df()
    

    an_df = pd.read_pickle(f"{path_to_pickle_dir}inst-{institution}_sub-{entities['subject']}_run-{entities['run']}_task-{entities['task']}_montage-{montage}_th{str(threshold).replace('.', '')}.pkl")
    win_idx_events_df = pd.read_pickle(f"{path_to_pickle_dir}inst-{institution}_sub-{entities['subject']}_run-{entities['run']}_task-{entities['task']}_montage-{montage}_win_idx_events_df.pkl")
    
    an_df = an_df[(an_df['run'] == int(entities['run']))
                  & (an_df['trial_type'].isin(['ENCODE', 'RECALL']))]
    win_idx_events_df = win_idx_events_df[(win_idx_events_df['trial_type'].isin(['ENCODE', 'RECALL']))]
    
    # Threshold the detections
    view_df = an_df
    view_df = view_df[(view_df['max_amplitude'] > amp)]# & (view_df['max_amplitude'] < amp[1])]
    view_df = view_df[(view_df['relative_midpoint'] > -1000000 * float(win_size)) & (view_df['relative_midpoint'] < 1000000 * float(win_size))]
    view_df = view_df[(view_df['cycles'] > cycles)]# & (view_df['cycles'] < cycles[1])]
    view_df = view_df[(view_df['freq_at_max'] > freq[0]) & (view_df['freq_at_max'] < freq[1])]
    view_df['time_bin'] = pd.cut(view_df['relative_midpoint'], bins, labels=bins[:-1])
    
    # Assign presentation order
    win_idx_ch_df = generate_word_idx_df(an_df, win_idx_events_df)
    view_df = view_df.merge(win_idx_ch_df.loc[:, ['win_idx', 'pres_order', 'channel']])
    view_df = view_df.loc[~view_df['pres_order'].isna()]
    view_df['pres_order'] = view_df['pres_order'].astype(int)
    
    bin_counts_list_df.append(view_df)
    
    # ----- Determine significant channels -----
    
    # ----- Time bin method -----
    stats_results = []
    for time_bin in view_df['time_bin'].dtype.categories:
        bin_counts = view_df.loc[(view_df['time_bin'] == time_bin) & (view_df['trial_type'] == 'ENCODE'), :].groupby('channel').size().to_frame('count').reset_index()
        mean_bin_count = bin_counts['count'].mean()
        std_bin_count = bin_counts['count'].std()
        sig_channels = bin_counts.loc[bin_counts['count'] >  mean_bin_count + 3 * std_bin_count, 'channel']
        
        for sig_channel in sig_channels:
            stats_results.append([sig_channel, time_bin])
            
    time_bin_sig_df = pd.DataFrame(stats_results, columns=['channel', 'time_bin'])
    time_bin_sig_channels_df = time_bin_sig_df.groupby('channel').size().to_frame('n_sig_bins').reset_index()
            
    time_bin_sig_df['subject'] = int(entities['subject'])
    time_bin_sig_df['task'] = entities['task']
    time_bin_sig_df['run'] = int(entities['run'])
    
    time_bin_sig_list_bins_df.append(time_bin_sig_df)
    
    time_bin_sig_channels_df['subject'] = int(entities['subject'])
    time_bin_sig_channels_df['task'] = entities['task']
    time_bin_sig_channels_df['run'] = int(entities['run'])
    
    time_bin_sig_channels_list_df.append(time_bin_sig_channels_df)

    grouped_df = view_df.groupby(['text', 'channel', 'time_bin']).size().to_frame('count').reset_index()
    grouped_df = grouped_df.merge(win_idx_ch_df, how='right')
    grouped_df['count'] = grouped_df['count'].fillna(0)

    for sig_channel in time_bin_sig_channels_df['channel'].unique():

        chan_df = grouped_df[grouped_df['channel'] == sig_channel]
        chan_df['task'] = time_bin_sig_channels_df['task'].unique()[0]
        chan_df['ch_selection'] = 'z-score'

        sig_channels_words_df.append(chan_df)
            
        
all_time_bin_sig_bins = pd.concat(time_bin_sig_list_bins_df)
all_time_bin_sig_channels = pd.concat(time_bin_sig_channels_list_df)
sig_channels_words_df = pd.concat(sig_channels_words_df)
bin_counts_df = pd.concat(bin_counts_list_df)

bin_counts_df['pres_order'] = bin_counts_df['pres_order'].astype('category')
bin_counts_df = bin_counts_df.groupby(['pres_order', 'trial_type', 'task', 'subject', 'channel', 'run', 'time_bin', 'text'], observed=True).size().to_frame('HFO_count').reset_index()


all_time_bin_sig_channels = all_time_bin_sig_channels.merge(anat_df, on=('channel', 'subject'))
sig_channels_words_df = sig_channels_words_df.merge(anat_df, on=('channel', 'subject'))

all_time_bin_sig_bins['task'] = all_time_bin_sig_bins['task'].astype('category')
all_time_bin_sig_channels['task'] = all_time_bin_sig_channels['task'].astype('category')
sig_channels_words_df['task'] = sig_channels_words_df['task'].astype('category')

bin_counts_df['task'] = bin_counts_df['task'].astype('category')
bin_counts_df['trial_type'] = bin_counts_df['trial_type'].astype('category')

all_time_bin_sig_bins.to_pickle(f'/home/jan_cimbalnik/Nextcloud_work/Science/Projects/memory_encoding/pickles/temp/all_time_bin_sig_bins_inst-{institution}_mont-{montage}.pkl')
all_time_bin_sig_channels.to_pickle(f'/home/jan_cimbalnik/Nextcloud_work/Science/Projects/memory_encoding/pickles/temp/all_time_bin_sig_channels_inst-{institution}_mont-{montage}.pkl')
sig_channels_words_df.to_pickle(f'/home/jan_cimbalnik/Nextcloud_work/Science/Projects/memory_encoding/pickles/temp/all_sig_channels_words_df_inst-{institution}_mont-{montage}.pkl')
bin_counts_df.to_pickle(f'/home/jan_cimbalnik/Nextcloud_work/Science/Projects/memory_encoding/pickles/temp/bin_counts_df_inst-{institution}_mont-{montage}.pkl')
