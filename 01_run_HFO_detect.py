#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Nov 21 15:49:50 2019

Script to run EPYCOM algorithms on memory encoding dataset

Ing.,Mgr. (MSc.) Jan Cimbálník
Biomedical engineering
International Clinical Research Center
St. Anne's University Hospital in Brno
Czech Republic
&
Mayo systems electrophysiology lab
Mayo Clinic
200 1st St SW
Rochester, MN
United States
"""

import os, argparse, re
from time import time

import numpy as np
import pandas as pd

from bids import BIDSLayout, BIDSLayoutIndexer
from epycom.event_detection import HilbertDetector
from pymef import MefSession

# from pathlib import Path
# file_path = Path(__file__).parent.absolute()

# %% Parse arguments
    
parser = argparse.ArgumentParser(description='Memory encoding task detection of HFO')

parser.add_argument('path_to_dataset', action="store",
                    nargs='?', type=str, help='Specify BIDS dataset path')
parser.add_argument('--subjects', default=None, action="store",
                    nargs='+', type=int, help='Specify BIDS subject')
parser.add_argument('--sessions', default=None, action="store",
                    nargs='+',type=int, help='Specify BIDS session')
parser.add_argument('--tasks', default=None, action="store",
                    nargs='+',type=str, help='Specify BIDS task')
parser.add_argument('--runs', default=None, action="store",
                    nargs='+',type=int, help='Specify BIDS run')

parser.add_argument('--channels', default=None, action="store",
                    nargs='+',type=str, help='Specify iEEG channels run')

parser.add_argument('--montage', default='unipolar', action="store",
                    type=str, help='Specify montage for processing (unipolar, bipolar, wm)')

parser.add_argument('--n_cores', default=None, action="store", type=int, help='Specify number of cores for multiprocessing')


parsed_vals = parser.parse_args()

print(parsed_vals)

# subjects = ['007']
# sessions = None
# tasks = ['FR']
# runs = None

# channels = None
# n_cores = 4

path_to_dataset = parsed_vals.path_to_dataset
subjects = parsed_vals.subjects
sessions = parsed_vals.sessions
tasks = parsed_vals.tasks
runs = parsed_vals.runs

channels = parsed_vals.channels

montage = parsed_vals.montage

n_cores = parsed_vals.n_cores

# %% Presets

if 'fnusa' in path_to_dataset:
    fs = 5000  # We know this for iEEG
elif 'mayo' in path_to_dataset:
    fs = 32000
elif 'wroclaw' in path_to_dataset:
    fs = 4000

if not path_to_dataset.endswith('/'):
    path_to_dataset += '/'
path_to_source = path_to_dataset+'sourcedata/'
path_to_derivatives = path_to_dataset+'derivatives/'
os.makedirs(path_to_derivatives, exist_ok=True)

detect_on = ['events', 'whole_rec', 'recall']

mef_pwd = None

pre_offset = 1.25  # in seconds
post_offset = 1.25  # in seconds

win_size = 10 # in seconds

threshold = 2.5

compute_instance = HilbertDetector(low_fc=60,
                                   high_fc=800,
                                   band_spacing='log',
                                   num_bands=100,
                                   cyc_th=1,
                                   threshold=threshold)

# %% Processing - in small windows (2.5 seconds)
"""
The middle point for the window is:
COUNTDOWN - appearance of the countdown digit
ENCODING - appearance of the word(s)
DISTRACTOR - appearance of the equation
RECALL - start of word vocalization
"""

# Get data files
mefd_pattern = re.compile(r'\.mefd$')
indexer = BIDSLayoutIndexer(ignore=[mefd_pattern, 'sourcedata', 'derivatives', 'code'])
l = BIDSLayout(path_to_dataset, indexer=indexer,
               database_path=path_to_dataset+'.sql', reset_database=True)
filter_dict = {'suffix': 'ieeg',
               'extension': 'json'}
if subjects is not None:
    filter_dict['subject'] = [str(x).zfill(3) for x  in subjects]
if sessions is not None:
    filter_dict['session'] = [str(x).zfill(3) for x in sessions]
if tasks is not None:
    filter_dict['task'] = tasks
if runs is not None:
    filter_dict['run'] = runs
json_files = l.get(**filter_dict)

mef_sessions = []
channel_dfs = []
event_dfs = []
for json_file in json_files:
    json_entities = json_file.entities
    
    # Get valid channels
    channel_file = l.get(suffix='channels', extension='tsv',
                          task=json_entities['task'],
                          subject=json_entities['subject'],
                          session=json_entities['session'],
                          run=json_entities['run'])[0]
    channel_df = channel_file.get_df()
    channel_dfs.append(channel_df[channel_df['type'].isin(['SEEG', 'ECOG'])])
    
    
    # Get events of interest
    events_file = l.get(suffix='events', extension='tsv',
                        task=json_entities['task'],
                        subject=json_entities['subject'],
                        session=json_entities['session'],
                        run=json_entities['run'])[0]
    events_df = events_file.get_df()
    # events_df = events_df.loc[~events_df['trial_type'].isna(), ['onset', 'sample', 'duration', 'trial_type']]
    event_dfs.append(events_df.loc[:, ['onset', 'sample', 'trial_type', 'value']])
    
    mef_session_path = os.path.splitext(json_file.path)[0]+'.mefd'
    mef_sessions.append(MefSession(mef_session_path, mef_pwd))
    
# # Construct channel df in order to get contacts in the hippocampus
# filter_dict.update({'extension': 'tsv',
#                     'suffix': 'electrodes'})
# filter_dict.pop('task')

# electrode_files = l.get(**filter_dict)
# all_ch_list = []
# for ef in electrode_files:
#     el_entities = ef.entities
#     el_df = ef.get_df()
#     el_df['subject'] = int(el_entities['subject'])
#     all_ch_list.append(el_df)
    
# all_el_df = pd.concat(all_ch_list)

# el_df = all_el_df[all_el_df['anatomy_structure'] != 'ex']

if montage == 'unipolar':    

    for ms, ch_df, ev_df, json_file in zip(mef_sessions, channel_dfs, event_dfs, json_files):
            
        print(f"Working on {json_file.filename[:-5]}")    
        
        json_entities = json_file.entities
    
        ttl_df = ev_df.loc[ev_df['trial_type'].isna(), :]
        ev_df = ev_df.loc[~ev_df['trial_type'].isna(), :]
        
        # Get uUTC unique times (PAL task can have the times doubled)
        uq_times = ev_df['onset'].unique()
        uq_times *= 1e6
        uq_uutc_times = (uq_times + ms.session_md['session_specific_metadata']['earliest_start_time'][0]).astype(int)
        uq_samples = ev_df['sample'].unique()
            
        # Basic channel info
        bi = ms.read_ts_channel_basic_info()
        
        # Iterate over channels
        all_chan_seg_df_list = []
        all_chan_whole_df_list = []
        all_chan_recalls_df_list = []
        for ch in list(ch_df['name']):
            
            if channels is not None:
                if ch not in channels:
                    continue

            if ch not in[x['name'] for x in ms.read_ts_channel_basic_info()]:
                continue

            print(f"\tDetecting channel {ch}")

            data = ms.read_ts_channels_sample(ch, [None, None])
            fs = int([x['fsamp'] for x in bi if x['name'] == ch][0][0])
            ch_start_time = [x['start_time'] for x in bi if x['name'] == ch][0][0]
            compute_instance.params['fs'] = fs

            # Samp offsets
            pre_samp_offset = int(pre_offset*fs)
            post_samp_offset = int(post_offset*fs)

            # Since our window is constant we can create "artifical" channel by gluing the segments together
            starts = [x-pre_samp_offset for x in uq_samples if ((x-pre_samp_offset) > 0 and (x+post_samp_offset) < len(data))]
            stops = [x+post_samp_offset for x in uq_samples if ((x-pre_samp_offset) > 0 and (x+post_samp_offset) < len(data))]

            idx_arr = np.concatenate([np.arange(x, y) for x, y in zip(starts, stops)])

            seg_data = data[idx_arr]

            t = time()

            # ----- Segmented -----

            # Run windowed function over the segmented data
            res = compute_instance.run_windowed(seg_data,
                                                window_size=pre_samp_offset+post_samp_offset,
                                                n_cores=n_cores)
            res_df = pd.DataFrame(res)

            # Correct the window starts and stops based on sample starts/stops
            for win_idx in res_df.win_idx.unique():
                win_start = starts[win_idx]
                res_df.loc[res_df['win_idx'] == win_idx, ['event_start', 'event_stop']] += win_start

            res_df['event_start'] = (((res_df['event_start']/fs) * 1e6) + ch_start_time).astype(int)
            res_df['event_stop'] = (((res_df['event_stop']/fs) * 1e6) + ch_start_time).astype(int)

            # Assign channel
            res_df['channel'] = ch


            all_chan_seg_df_list.append(res_df)

            # ----- Whole recording

            # Run windowed function over the whole recroding
            whole_res = compute_instance.run_windowed(data,
                                                window_size=win_size*fs,
                                                n_cores=n_cores)
            whole_res_df = pd.DataFrame(whole_res)

            # Correct the window starts and stops based on sample starts/stops
            for win_idx in whole_res_df.win_idx.unique():
                whole_res_df.loc[whole_res_df['win_idx'] == win_idx, ['event_start', 'event_stop']] += win_idx*win_size*fs

            whole_res_df['event_start'] = (((whole_res_df['event_start']/fs) * 1e6) + ch_start_time).astype(int)
            whole_res_df['event_stop'] = (((whole_res_df['event_stop']/fs) * 1e6) + ch_start_time).astype(int)

            # Assign channel
            whole_res_df['channel'] = ch

            all_chan_whole_df_list.append(whole_res_df)

            # ----- Recall segments (only FR and PAL)-----
            if json_entities['task'] in ['FR', 'PAL']:
                # Find all recording starts
                rec_starts = ttl_df.loc[ttl_df['value'] == 232]
                rec_stops = ttl_df.loc[ttl_df['value'] == 233]

                # Determine the ones belonging to recall - every third recording start and stop
                # !!! This should be veryfied for the older recordings!!!!
                recall_idxs = range(2, len(rec_starts), 3)
                recall_starts = rec_starts.iloc[recall_idxs, :]
                recall_stops = rec_stops.iloc[recall_idxs, :]

                starts = [x-pre_samp_offset for x in recall_starts['sample']]
                stops = [x+post_samp_offset-1 for x in recall_stops['sample']]

                recalls_res_df_list = []
                for start, stop in zip(starts, stops):
                    if start < 0 or stop > len(data):
                        continue
                    idx_arr = np.arange(start, stop)
                    recall_data = data[idx_arr]

                    res = compute_instance.run_windowed(recall_data,
                                                        window_size=pre_samp_offset+post_samp_offset,
                                                        n_cores=n_cores)
                    recall_res_df = pd.DataFrame(res)

                    for win_idx in recall_res_df.win_idx.unique():
                        recall_res_df.loc[recall_res_df['win_idx'] == win_idx, ['event_start', 'event_stop']] += win_idx*win_size*fs

                    recall_res_df['event_start'] = (((recall_res_df['event_start']/fs) * 1e6) + ch_start_time).astype(int)
                    recall_res_df['event_stop'] = (((recall_res_df['event_stop']/fs) * 1e6) + ch_start_time).astype(int)

                    recalls_res_df_list.append(recall_res_df)

                recalls_res_df = pd.concat(recalls_res_df_list)
                recalls_res_df['channel'] = ch

                all_chan_recalls_df_list.append(recalls_res_df)

            print(f"\tProcessed in {time()-t} s")



        # Save the dataframe (on disk for now)
        all_chan_seg_df = pd.concat(all_chan_seg_df_list)
        all_chan_whole_df = pd.concat(all_chan_whole_df_list)
        sub = json_entities['subject']
        ses = json_entities['session']
        run = json_entities['run']
        task = json_entities['task']
        all_chan_seg_df.to_pickle(f"{path_to_derivatives}/{montage}/sub-{sub}_ses-{ses}_run-{run}_task-{task}_th-{threshold}_mont-{montage}.pkl")
        all_chan_whole_df.to_pickle(f"{path_to_derivatives}/{montage}/sub-{sub}_ses-{ses}_run-{run}_task-{task}_th-{threshold}_mont-{montage}_whole_channel.pkl")
        if json_entities['task'] in ['FR', 'PAL']:
            all_chan_recalls_df = pd.concat(all_chan_recalls_df_list)
            all_chan_recalls_df.to_pickle(f"{path_to_derivatives}/{montage}/sub-{sub}_ses-{ses}_run-{run}_task-{task}_th-{threshold}_mont-{montage}_whole_recalls.pkl")
        
elif montage == 'bipolar':
    
    for ms, ch_df, ev_df, json_file in zip(mef_sessions, channel_dfs, event_dfs, json_files):

        ch_df = ch_df[~(ch_df['name'].str.replace('[^0-9]', '', regex=True) == '')]

        print(f"Working on {json_file.filename[:-5]}")    
        
        json_entities = json_file.entities
        
        ttl_df = ev_df.loc[ev_df['trial_type'].isna(), :]
        ev_df = ev_df.loc[~ev_df['trial_type'].isna(), :]
        
        # Get uUTC unique times (PAL task can have the times doubled)
        uq_times = ev_df['onset'].unique()
        uq_times *= 1e6
        uq_uutc_times = (uq_times + ms.session_md['session_specific_metadata']['earliest_start_time'][0]).astype(int)
        uq_samples = ev_df['sample'].unique()

        # Basic channel info
        bi = ms.read_ts_channel_basic_info()
        
        # Create channel pairs
        ch_df = ch_df.sort_values('name').reset_index(drop=True)
        
        # Create channel pairs    
        ch_df['group'] = ch_df['name'].str.replace(r'\d+', '', regex=True)
        ch_df['group'] = ch_df['group'].str.replace('_', '')
        ch_df['number'] = ch_df['name'].str.replace('[^0-9]', '', regex=True).astype(int)
        
        chan_pairs = []
        for i, r in ch_df.iterrows():
            second_chan = ch_df[(ch_df['group'] == r['group']) & (ch_df['number'] == r['number']+1)]
            if len(second_chan):
                chan_pairs.append([r['name'], second_chan['name'].values[0]])
            
        
        # Iterate over channels
        all_chan_seg_df_list = []
        all_chan_whole_df_list = []
        all_chan_recalls_df_list = []
        for ch_pair in chan_pairs:
            
            if ch_pair[0] not in ch_df['name'].unique() or ch_pair[1] not in ch_df['name'].unique():
                continue

            if ch_pair[0] not in[x['name'] for x in ms.read_ts_channel_basic_info()] or ch_pair[1] not in[x['name'] for x in ms.read_ts_channel_basic_info()]:
                continue

            print(f"\tDetecting channel {ch_pair[0]}-{ch_pair[1]}")

            data = ms.read_ts_channels_sample(ch_pair, [None, None])
            fs = int([x['fsamp'] for x in bi if x['name'] == ch_pair[0]][0][0])
            ch_start_time = [x['start_time'] for x in bi if x['name'] == ch_pair[0]][0][0]
            compute_instance.params['fs'] = fs

            if len(data[0]) != len(data[1]):
                continue

            data = data[0]-data[1]
                    
            # Samp offsets
            pre_samp_offset = int(pre_offset*fs)
            post_samp_offset = int(post_offset*fs)
            
            # Since our window is constant we can create "artifical" channel by gluing the segments together
            starts = [x-pre_samp_offset for x in uq_samples if ((x-pre_samp_offset) > 0 and (x+post_samp_offset) < len(data))]
            stops = [x+post_samp_offset for x in uq_samples if ((x-pre_samp_offset) > 0 and (x+post_samp_offset) < len(data))]

            idx_arr = np.concatenate([np.arange(x, y) for x, y in zip(starts, stops)])
            
            seg_data = data[idx_arr]
                    
            t = time()
            
            # ----- Segmented -----
            
            # # Run windowed function over the segmented data
            # res = compute_instance.run_windowed(seg_data,
            #                                     window_size=pre_samp_offset+post_samp_offset,
            #                                     n_cores=n_cores)
            # res_df = pd.DataFrame(res)
            #
            # # Correct the window starts and stops based on sample starts/stops
            # for win_idx in res_df.win_idx.unique():
            #     win_start = starts[win_idx]
            #     res_df.loc[res_df['win_idx'] == win_idx, ['event_start', 'event_stop']] += win_start
            #
            # res_df['event_start'] = (((res_df['event_start']/fs) * 1e6) + ch_start_time).astype(int)
            # res_df['event_stop'] = (((res_df['event_stop']/fs) * 1e6) + ch_start_time).astype(int)
            #
            # # Assign channel
            # res_df['channel'] = '-'.join(ch_pair)
            #
            # all_chan_seg_df_list.append(res_df)
            
            # ----- Whole recording

            # Run windowed function over the whole recroding
            whole_res = compute_instance.run_windowed(data,
                                                window_size=win_size*fs,
                                                n_cores=n_cores)
            whole_res_df = pd.DataFrame(whole_res)

            # Correct the window starts and stops based on sample starts/stops
            for win_idx in whole_res_df.win_idx.unique():
                whole_res_df.loc[whole_res_df['win_idx'] == win_idx, ['event_start', 'event_stop']] += win_idx*win_size*fs

            whole_res_df['event_start'] = (((whole_res_df['event_start']/fs) * 1e6) + ch_start_time).astype(int)
            whole_res_df['event_stop'] = (((whole_res_df['event_stop']/fs) * 1e6) + ch_start_time).astype(int)

            # Assign channel
            whole_res_df['channel'] = '-'.join(ch_pair)

            all_chan_whole_df_list.append(whole_res_df)

            # ----- Recall segments (only FR and PAL)-----
            if json_entities['task'] in ['FR', 'PAL']:
                # Find all recording starts
                rec_starts = ttl_df.loc[ttl_df['value'] == 232]
                rec_stops = ttl_df.loc[ttl_df['value'] == 233]

                # Determine the ones belonging to recall - every third recording start and stop
                # !!! This should be veryfied for the older recordings!!!!
                recall_idxs = range(2, len(rec_starts), 3)
                recall_starts = rec_starts.iloc[recall_idxs, :]
                recall_stops = rec_stops.iloc[recall_idxs, :]

                starts = [x-pre_samp_offset for x in recall_starts['sample']]
                stops = [x+post_samp_offset-1 for x in recall_stops['sample']]

                recalls_res_df_list = []
                for start, stop in zip(starts, stops):
                    if start < 0 or stop > len(data):
                        continue
                    idx_arr = np.arange(start, stop)
                    recall_data = data[idx_arr]

                    res = compute_instance.run_windowed(recall_data,
                                                        window_size=pre_samp_offset+post_samp_offset,
                                                        n_cores=n_cores)
                    recall_res_df = pd.DataFrame(res)

                    for win_idx in recall_res_df.win_idx.unique():
                        recall_res_df.loc[recall_res_df['win_idx'] == win_idx, ['event_start', 'event_stop']] += win_idx*win_size*fs

                    recall_res_df['event_start'] = (((recall_res_df['event_start']/fs) * 1e6) + ch_start_time).astype(int)
                    recall_res_df['event_stop'] = (((recall_res_df['event_stop']/fs) * 1e6) + ch_start_time).astype(int)

                    recalls_res_df_list.append(recall_res_df)

                recalls_res_df = pd.concat(recalls_res_df_list)
                recalls_res_df['channel'] = '-'.join(ch_pair)

                all_chan_recalls_df_list.append(recalls_res_df)
            
            print(f"\tProcessed in {time()-t} s")
            
        # Save the dataframe (on disk for now)
        all_chan_seg_df = pd.concat(all_chan_seg_df_list)

        #all_chan_whole_df = pd.concat(all_chan_whole_df_list)
        sub = json_entities['subject']
        ses = json_entities['session']
        run = json_entities['run']
        task = json_entities['task']
        all_chan_seg_df.to_pickle(f"{path_to_derivatives}/{montage}/sub-{sub}_ses-{ses}_run-{run}_task-{task}_th-{threshold}_mont-{montage}.pkl")
        # all_chan_whole_df.to_pickle(f"{path_to_derivatives}/{montage}/sub-{sub}_ses-{ses}_run-{run}_task-{task}_th-{threshold}_mont-{montage}_whole_channel.pkl")
        # if json_entities['task'] in ['FR', 'PAL']:
        #     all_chan_recalls_df = pd.concat(all_chan_recalls_df_list)
        #     all_chan_recalls_df.to_pickle(f"{path_to_derivatives}/{montage}/sub-{sub}_ses-{ses}_run-{run}_task-{task}_th-{threshold}_mont-{montage}_recalls.pkl")


            
